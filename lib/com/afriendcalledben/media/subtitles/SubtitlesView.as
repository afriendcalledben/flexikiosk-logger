package com.afriendcalledben.media.subtitles 
{
	import com.afriendcalledben.style.Padding;
	import com.afriendcalledben.text.TextLabel;
	import flash.display.Sprite;
	import nl.inlet42.data.subtitles.SubTitleData;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class SubtitlesView extends Sprite
	{
		private var _width:Number;
		private var _height:Number;
		private var _padding:Padding;
		private var _subtitles:Array;
		private var _currentText:String = '';
		
		public function SubtitlesView(w:int, h:int, padding:Padding = null) 
		{
			_width = w;
			_height = h;
			_padding = (padding) ? padding : new Padding(0, 0, 0, 0);
		}
		
		public function setSubtitles(text:String):void
		{
			if (_currentText == text) return;
			_currentText = text;
			trace(text);
			
			while (numChildren > 0) {
				removeChildAt(0);
			}
			
			if (text.length > 0) {
				var arr:Array = text.split("\r");
				text = arr.join('');
				arr = text.split("\n");
				var ypos:int = _height - _padding.bottom;
				for (var i:int = arr.length - 1; i >= 0; i--) {
					var sline:SubtitleLine = new SubtitleLine(arr[i]);
					sline.x = (_width * .5) - (sline.width * .5);
					sline.y = ypos - sline.height;
					addChild(sline);
					ypos -= sline.height + 10;
				}
			}
		}
	}

}