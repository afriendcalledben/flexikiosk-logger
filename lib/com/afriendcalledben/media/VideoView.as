/**
* Flash AS3 Video Instance
* @author Ben Tandy
* @version 1.0
*/
package com.afriendcalledben.media
{
	import flash.display.Sprite;
	
	import flash.utils.Timer;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	
	import flash.media.Video;
	import flash.media.SoundTransform;
	
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	import com.afriendcalledben.events.VideoEvent;
	
	public class VideoView extends Sprite
	{
		
		private var _videoPath:String;
		
		private var _video:Video;
		private var _videoStream:NetStream;
		private var _videoConn:NetConnection;
		private var _videoClient:Object;
		private var _duration:Number = NaN;
		private var _videoCued:Boolean = false;
		
		private var _videoStartTimer:Timer;
		
		/**
		 * Creates an instance of a video view
		 * @param path		Absolute or relative path to the video file
		 * @param vwidth	Desired width of the video view
		 * @param vheight	Desired height of the video view
		 */
		public function VideoView(path:String = "", vwidth:Number = 320, vheight:Number = 240)
		{
			if (path != "") // If path provided, initialise video view.
			{
				init(path, vwidth, vheight);
			}
		}
		
		/**
		 * Creates an instance of a video view
		 * @param path		Absolute or relative path to the video file
		 * @param vwidth	Desired width of the video view
		 * @param vheight	Desired height of the video view
		 * @param vvolume	Desired initial volume of the video view
		 */
		public function init(path:String, vwidth:Number = 320, vheight:Number = 240, vvolume:Number = 1):void
		{
			
			_videoPath = path;
			
			_video = new Video(vwidth, vheight);
			addChild(_video);
			
			// Create a NetConnection and a NetStream
			_videoConn = new NetConnection();
			_videoConn.connect(null);
			_videoStream = new NetStream(_videoConn);

			_video.attachNetStream(_videoStream);
			
			_videoStream.addEventListener(NetStatusEvent.NET_STATUS, onVideoStatus);
			_videoClient = new Object();
			_videoClient.onMetaData = onMetaRecieved; // Called on Metadata from the FLV recieved - mainly for duration.
			_videoStream.client = _videoClient;
			
			var st:SoundTransform = new SoundTransform(); // Sets the initial volume.
			st.volume = vvolume;
			_videoStream.soundTransform = st;
			
		}
		
		/**
		 * Cues up the video at the beginning for instant playback.
		 */
		public function cueVideo(pos:int = 0):void
		{
			_videoCued = false;
			_videoStartTimer = new Timer(10, 0);
			_videoStartTimer.addEventListener(TimerEvent.TIMER, checkStart);
			_videoStartTimer.start();
			_videoStream.resume();
			_videoStream.seek(pos);
			_videoStream.play(_videoPath);
			
		}
		
		private function checkStart(e:Event):void
		{
			
			if (_videoStream.time > 0 && _videoStream.time < 5) {
				
				_videoStream.pause();
				_videoStream.seek(0);
				_videoStartTimer.stop();
				_videoStartTimer.removeEventListener(TimerEvent.TIMER, checkStart);
				_videoStartTimer = null;
				_videoCued = true;
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_CUED));
				
			}
			
		}
		
		/**
		 * Plays the video from a paused position.
		 */
		public function play():void
		{
			
			_videoStream.seek(0);
			_videoStream.resume();
		}
			
		/**
		 * Starts the video.
		 */
		public function start():void
		{
			
			_videoStream.play(_videoPath);
			
		}
		
		/**
		 * Pauses the video.
		 */
		public function pause():void
		{
			
			_videoStream.pause();
			
		}
		
		/**
		 * Resumes the video.
		 */
		public function resume():void
		{
			
			_videoStream.resume();
			
		}
		
		/**
		 * Seeks a postition in the video.
		 * @param time		Position in the video in seconds.
		 */
		public function seek(time:Number):void
		{
			
			_videoStream.seek(time);
			
		}
		
		private function onVideoStatus(stats:NetStatusEvent):void
		{
			
			switch (stats.info.code)
			{
				
				case "NetStream.Buffer.Empty" :	
				dispatchEvent(new VideoEvent(VideoEvent.BUFFER_EMPTY));
				break;
				case "NetStream.Buffer.Full" :	
				dispatchEvent(new VideoEvent(VideoEvent.BUFFER_FULL));
				break;
				case "NetStream.Buffer.Flush" :	
				dispatchEvent(new VideoEvent(VideoEvent.BUFFER_FLUSH));
				break;
				
				case "NetStream.Play.Start" :	
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_STARTED));
				break;
				case "NetStream.Play.Stop" :	
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_STOPPED));
				break;
				
				case "NetStream.Play.StreamNotFound" :	
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_NOT_FOUND));
				break;
				
				case "NetStream.Pause.Notify" :	
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_PAUSED));
				break;
				case "NetStream.Unpause.Notify" :	
				dispatchEvent(new VideoEvent(VideoEvent.VIDEO_UNPAUSED));
				break;
				
			}
			
		}
		
		private function onMetaRecieved(meta:Object):void
		{
			trace(meta.duration);
			
				var obj:Object = new Object();
				obj.duration = meta;
				dispatchEvent(new VideoEvent(VideoEvent.META_RECIEVED, obj));
				_duration = meta.duration;
			
		}
		
		/**
		* The current position of the video in seconds.
		*/
		public function get cued():Boolean
		{
			
			return _videoCued;
			
		}
		
		/**
		* The current position of the video in seconds.
		*/
		public function get position():Number
		{
			
			return _videoStream.time;
			
		}
		
		/**
		* The total length of the video in seconds.
		*/
		public function get duration():Number
		{
			
			return _duration;
			
		}
		
		/**
		* Disposes of all assets in the instance.
		*/
		public function dispose():void
		{
			if (_videoStartTimer != null) {
				_videoStartTimer.stop();
				_videoStartTimer.removeEventListener(TimerEvent.TIMER, checkStart);
				_videoStartTimer = null;
			}
			
			if (_videoStream != null) {
				_videoStream.pause();
				_videoStream.close();
				_videoStream.removeEventListener(NetStatusEvent.NET_STATUS, onVideoStatus);
			}
			
			_videoStream = null;
			_videoConn = null
			_videoClient = null;
			
		}
		
	}
	
}