package com.afriendcalledben.services 
{
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class BitmapResizer 
	{
		public static function getImageBitmapDataWithinRect(sourceBD:BitmapData, rect:Rectangle, fillGap:Boolean = true):BitmapData
		{
			var imgRect:Rectangle = new Rectangle(0, 0, sourceBD.width, sourceBD.height);
			trace(sourceBD.width);
			var matrix:Matrix = fitIntoRect(imgRect, rect, false, fillGap);
			var w:Number = (fillGap) ? rect.width : Math.floor(sourceBD.width * matrix.a);
			var h:Number = (fillGap) ? rect.height : Math.floor(sourceBD.height * matrix.d);
			if (w > 0 && h > 0) {
				var bd:BitmapData = new BitmapData(w, h, true, 0x00000000);
				bd.draw(sourceBD, matrix, null, null, null, true);
				return bd;
			} else {
				return sourceBD;
			}
		}
		
		public static function getImageBitmapDataFillingRect(sourceBD:BitmapData, rect:Rectangle):BitmapData
		{
			var imgRect:Rectangle = new Rectangle(0, 0, sourceBD.width, sourceBD.height);
			var matrix:Matrix = fitIntoRect(imgRect, rect, true);
			var bd:BitmapData = new BitmapData(rect.width, rect.height, true, 0x00000000);
			bd.draw(sourceBD, matrix, null, null, null, true);
			return bd;
		}
		
		private static function fitIntoRect(imageRect : Rectangle , boundsRect : Rectangle, fillRect : Boolean = true, translate:Boolean = true) : Matrix
		{
			var matrix : Matrix = new Matrix();
			
			var wD : Number = imageRect.width;
			var hD : Number = imageRect.height;
			
			var wR : Number = boundsRect.width;
			var hR : Number = boundsRect.height;
			
			var sX : Number = wR / wD;
			var sY : Number = hR / hD;
			
			var rD : Number = wD / hD;
			var rR : Number = wR / hR;
			
			var sH : Number = fillRect ? sY : sX;
			var sV : Number = fillRect ? sX : sY;
			
			var s : Number = rD >= rR ? sH : sV;
			var w : Number = wD * s;
			var h : Number = hD * s;
			
			var tX : Number = 0.0;
			var tY : Number = 0.0;
			
			tX = 0.5 * (w - wR);
			tY = 0.5 * (h - hR);
			
			matrix.scale(s, s);
			if (translate) matrix.translate(boundsRect.left - tX, boundsRect.top - tY);
			
			return matrix;
		}
		
	}

}