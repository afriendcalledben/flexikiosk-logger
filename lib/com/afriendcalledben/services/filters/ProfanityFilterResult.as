package com.afriendcalledben.services.filters {

	/**
	 * @author eric decker : firstborn : 2009
	 */

	public class ProfanityFilterResult {
		
		internal var _clean:Boolean;
		internal var _words:Array;
		internal var _matches:Array;
		internal var _indexes:Array;
		
		public function ProfanityFilterResult() {
		}

		/** @return true if no matches were found **/
		public function get clean():Boolean {
			return _clean;
		}
		
		/** @return list of matching words as defined from list **/
		public function get words():Array {
			return _words;
		}
		
		/** @return list of matching words from sampled text **/
		public function get matches():Array {
			return _matches;	
		}
		
		/** @return list of indexes of matching words **/
		public function get indexes():Array {
			return _indexes;
		}
		
	}
	
}
