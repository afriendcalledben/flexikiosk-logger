package com.afriendcalledben.services 
{
	import com.greensock.loading.LoaderMax;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import com.greensock.loading.ImageLoader;
	import com.greensock.events.LoaderEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class BitmapManager
	{
		public static var LOADED:String = 'bitmaps_loaded';
		
    	protected static var disp:EventDispatcher;
		private static var _dict:Dictionary = new Dictionary();
		private static var _xmlList:XMLList;
		
		public static function setXML(xmlList:XMLList):void
		{
			_xmlList = xmlList;
			
			var queue:LoaderMax = new LoaderMax( { onComplete:onBitmapsLoaded } );
			for (var i:int = 0; i < _xmlList.length(); i++) {
				queue.append(new ImageLoader(_xmlList[i], {name:_xmlList[i].@id}));
			}
			queue.load();
		}
		
		private static function onBitmapsLoaded(e:LoaderEvent):void
		{
			for (var i:int = 0; i < _xmlList.length(); i++) {
				// IMAGE
				var id:String = _xmlList[i].@id;
				var bmp:Bitmap = LoaderMax.getContent(id).rawContent;
				// Generate full-size BitmapData
				var bd:BitmapData = new BitmapData(bmp.width, bmp.height, true, 0x00000000);
				bd.draw(bmp, null, null, null, null, true);
				_dict[id] = bd;
			}
			dispatchEvent(new Event(LOADED));
		}
		
		public static function getBitmapByID(id:String):BitmapData
		{
			return _dict[id];
		}
		
		public static function addEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false, p_priority:int=0, p_useWeakReference:Boolean=false):void {
			if (disp == null) { disp = new EventDispatcher(); }
			disp.addEventListener(p_type, p_listener, p_useCapture, p_priority, p_useWeakReference);
		}
		
		public static function removeEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false):void {
			if (disp == null) { return; }
			disp.removeEventListener(p_type, p_listener, p_useCapture);
		}
		
		public static function dispatchEvent(p_event:Event):void {
			if (disp == null) { return; }
			trace('SAM');
			disp.dispatchEvent(p_event);
			trace('BAM');
		}
		
	}

}