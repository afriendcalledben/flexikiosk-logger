package com.afriendcalledben.text 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import com.greensock.BlitMask;
	import com.greensock.TweenMax;
	import com.afriendcalledben.display.FadeBox;
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.style.StyleManager;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.ThrowPropsPlugin;
	import com.greensock.easing.Strong;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class DraggableTextPanel extends Sprite
	{
		private var _content:Sprite;
		private var _tf:TextField;
		private var _blitMask:BlitMask;
		private var _fadeGap:int;
		private var _bounds:Rectangle;
		
		private var t1:uint, t2:uint, t3:uint, y1:Number, y2:Number, yOverlap:Number, yOffset:Number, yOrig:Number;
		
		public function DraggableTextPanel(txt:String, bounds:Rectangle, fadeGap:int = 50, fadeColor:Number = 0xFF0000) 
		{
			TweenPlugin.activate([ThrowPropsPlugin]);
			
			scrollRect = bounds;
			
			_bounds = bounds;
			_fadeGap = fadeGap;
			
			_content = new Sprite();
			addChild(_content);
			
			_tf = new TextField();
			_tf.width = bounds.width;
			_tf.embedFonts = _tf.multiline = _tf.wordWrap = true;
			StyleManager.setText(_tf, txt, 'center');
			_tf.y = _fadeGap;
			_content.addChild(_tf);
			
			if (_content.height > _bounds.height + (_fadeGap * 2)) {
			
				_blitMask = new BlitMask(_content, _bounds.x, _bounds.y, _bounds.width, _bounds.height, false, true);
				
				_blitMask.update(null, true);
				TweenMax.delayedCall(1, _blitMask.update, [null, true]);
				_blitMask.disableBitmapMode();
				
				_blitMask.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
				
				// Fades
				
				var topFade:FadeBox = new FadeBox(bounds.width, _fadeGap, 90, fadeColor);
				addChild(topFade);
				var bottomFade:FadeBox = new FadeBox(bounds.width, _fadeGap, -90, fadeColor);
				bottomFade.y = bounds.height - _fadeGap;
				addChild(bottomFade);
			}
		}

		private function mouseDownHandler(event:MouseEvent):void {
			_blitMask.enableBitmapMode();
			TweenMax.killTweensOf(_content);
			y1 = y2 = _content.y;
			yOffset = this.mouseY - _content.y;
			yOverlap = Math.max(0, (_content.height + (_fadeGap * 2)) - _bounds.height);
			t1 = t2 = getTimer();
			_content.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			_content.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}

		private function mouseMoveHandler(event:MouseEvent):void
		{
			var y:Number = this.mouseY - yOffset;
			//if mc's position exceeds the bounds, make it drag only half as far with each mouse movement (like iPhone/iPad behavior)
			if (y > _bounds.top) {
				_content.y = (y + _bounds.top) * 0.5;
			} else if (y < _bounds.top - yOverlap) {
				_content.y = (y + _bounds.top - yOverlap) * 0.5;
			} else {
				_content.y = y;
			}
			_blitMask.update();
			var t:uint = getTimer();
			//if the frame rate is too high, we won't be able to track the velocity as well, so only update the values 20 times per second
			if (t - t2 > 50) {
				y2 = y1;
				t2 = t1;
				y1 = _content.y;
				t1 = t;
			}
			event.updateAfterEvent();
		}

		private function mouseUpHandler(event:MouseEvent):void
		{
			_content.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			_content.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			var time:Number = (getTimer() - t2) / 1000;
			var yVelocity:Number = (_content.y - y2) / time;
			ThrowPropsPlugin.to(_content, {throwProps:{
										 y:{velocity:yVelocity, max:_bounds.top, min:_bounds.top - yOverlap, resistance:300}
									 }, onUpdate:_blitMask.update, onComplete:_blitMask.disableBitmapMode, ease:Strong.easeOut
									}, 10, 0.3, 1);
		}
		
	}

}