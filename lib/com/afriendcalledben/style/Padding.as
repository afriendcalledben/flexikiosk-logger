package com.afriendcalledben.style 
{
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Padding 
	{
		private var _top:Number;
		private var _right:Number;
		private var _bottom:Number;
		private var _left:Number;
		
		public function Padding(top:Number, right:Number = NaN, bottom:Number = NaN, left:Number = NaN) 
		{
			_top = top;
			_right = right;
			_bottom = bottom;
			_left = left;
		}
		
		public function get top():Number
		{
			return _top;
		}
		
		public function get right():Number
		{
			return _right;
		}
		
		public function get bottom():Number
		{
			return _bottom;
		}
		
		public function get left():Number
		{
			return _left;
		}
		
	}

}