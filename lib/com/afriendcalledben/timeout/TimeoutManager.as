package com.afriendcalledben.timeout 
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author ...
	 */
	public class TimeoutManager 
	{
		public static var TIMEOUT:String = 'timeout';
		private static var _stage:Stage;
		private static var _timeout:Number = 30;
    	protected static var disp:EventDispatcher;
		
		public static function setStage(s:Stage):void
		{
			_stage = s;
		}
		
		public static function setTimeout(value:Number):void
		{
			_timeout = value;
		}
		
		public static function activate():void
		{
			deactivate();
			_stage.addEventListener(MouseEvent.MOUSE_DOWN, startTimeout);
			startTimeout();
		}
		
		public static function deactivate():void
		{
			TweenMax.killDelayedCallsTo(doTimeout);
			_stage.removeEventListener(MouseEvent.MOUSE_DOWN, startTimeout);
		}
		
		private static function startTimeout(e:Event = null):void
		{
			TweenMax.killDelayedCallsTo(doTimeout);
			TweenMax.delayedCall(_timeout, doTimeout);
		}
		
		private static function doTimeout():void
		{
			dispatchEvent(new Event(TIMEOUT));
		}
		
		public static function addEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false, p_priority:int=0, p_useWeakReference:Boolean=false):void {
			if (disp == null) { disp = new EventDispatcher(); }
			disp.addEventListener(p_type, p_listener, p_useCapture, p_priority, p_useWeakReference);
		}
		
		public static function removeEventListener(p_type:String, p_listener:Function, p_useCapture:Boolean=false):void {
			if (disp == null) { return; }
			disp.removeEventListener(p_type, p_listener, p_useCapture);
		}
		
		public static function dispatchEvent(p_event:Event):void {
			if (disp == null) { return; }
			disp.dispatchEvent(p_event);
		}
		
	}

}