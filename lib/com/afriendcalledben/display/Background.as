package com.afriendcalledben.display 
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Ben Tandy (ben@afriendcalledben.com)
	 */
	public class Background extends Sprite
	{
		
		public function Background(w:int, h:int, color:Number = 0, alpha:Number = 1, backgroundImage:BitmapData = null, backgroundRepeat:Boolean = false) 
		{
			with (graphics) {
				beginFill(color);
				if (backgroundImage) beginBitmapFill(backgroundImage, null, backgroundRepeat, true);
				drawRect(0, 0, w, h);
				endFill();
			}
			
			this.alpha = alpha;
		}
		
	}

}