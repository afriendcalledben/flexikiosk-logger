package com.afriendcalledben.events 
{
	
    import flash.events.Event;

    public class CustomEvent extends Event
	{
		
		public var data:Object;
		
		/**
		 * Creates an instance of a CustomEvent.
		 * @param type		The type of event.
		 * @param obj		Additional data for the event.
		 */
		public function CustomEvent(type:String, obj:Object = null) {
			
			super(type, bubbles, cancelable);
			this.data = obj;
		
		}
		
		public override function clone() : Event {
			return new CustomEvent(type, data);
		}
	
	}
	
}