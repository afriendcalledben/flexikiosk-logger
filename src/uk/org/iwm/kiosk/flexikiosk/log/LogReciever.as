package uk.org.iwm.kiosk.flexikiosk.log
{ 
	import flash.events.EventDispatcher;
	import flash.events.StatusEvent;
    import flash.net.LocalConnection; 
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import flash.system.Security;
	
    public class LogReciever extends EventDispatcher
    { 
		private var _localConnection:LocalConnection = new LocalConnection();
		private var _connectionName:String;
		
        public function LogReciever(connectionName:String) 
        { 
			_connectionName = connectionName;
			
			_localConnection.allowDomain('*');
			_localConnection.client = this;
			_localConnection.addEventListener(StatusEvent.STATUS, onStatus);
			
            try 
            { 
                _localConnection.connect(connectionName);
				Log.log('LogReciever successfully connected');
				Log.log(_localConnection.domain);
            } 
            catch (error:ArgumentError) 
            { 
				Log.log('LogReciever failed to connect: '+error);
            } 
        }
		
        public function recieveLogEvent(message:String, logType:int, time:Date):void 
        {
			var evt:LogEvent = new LogEvent(message, logType, time);
            dispatchEvent(evt);
        }
        
        private function onStatus(event:StatusEvent):void {
            switch (event.level) {
                case "status":
                    Log.log("LocalConnection.send() succeeded");
                    break;
                case "error":
                    Log.log("LocalConnection.send() failed");
                    break;
            }
        }
    } 
}