package uk.org.iwm.kiosk.flexikiosk.logger.screens 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	import uk.org.iwm.kiosk.flexikiosk.logger.ui.LoadingIndicator;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class MainScreen extends Sprite
	{
		[Embed(source = "../../../../../../../../assets/images/iwm_logo.png")]
		private static var Gfx_iwm_logo:Class;
		
		private var _indicator:LoadingIndicator;
		
		public function MainScreen(w:int, h:int) 
		{
			var bmp:Bitmap = new Gfx_iwm_logo();
			bmp.x = (w / 2) - (bmp.width / 2) - 50;
			bmp.y = (h / 2) - (bmp.height / 2) - 25;
			addChild(bmp);
			
			_indicator = new LoadingIndicator();
			_indicator.x = (w / 2) - (_indicator.width / 2) - 50;
			_indicator.y = bmp.y + bmp.height + 20;
			_indicator.alpha = 0.5;
			_indicator.filters = [new GlowFilter(0xFFFFFF, 0.5)];
			addChild(_indicator);
		}
		
		public function set loading(value:Boolean):void
		{
			if (value) {
				_indicator.startAnimate();
			} else {
				_indicator.stopAnimate();
			}
		}
		
	}

}