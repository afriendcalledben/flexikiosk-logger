package uk.org.iwm.kiosk.flexikiosk.logger.screens 
{
	import flash.display.Sprite;
	import uk.org.iwm.kiosk.flexikiosk.log.LogConsole;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import uk.org.iwm.kiosk.flexikiosk.log.LogReciever;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class LogScreen extends Sprite
	{
		private var _logConsole:LogConsole;
		
		public function LogScreen(w:int, h:int) 
		{
			_logConsole = new LogConsole(w - 40, h - 40);
			_logConsole.x = _logConsole.y = 20;
			addChild(_logConsole);
		
			var logReciever:LogReciever = new LogReciever('iwm_logging');
			logReciever.addEventListener(LogEvent.LOG, onLog);
		}
		
		private function onLog(e:LogEvent):void
		{
			_logConsole.addLogEvent(e);
		}
		
	}

}