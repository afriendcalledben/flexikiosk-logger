package uk.org.iwm.kiosk.flexikiosk.logger
{
	import com.afriendcalledben.display.ShapeRect;
	import com.afriendcalledben.keyboard.KeyCode;
	import com.greensock.TweenMax;
	import flash.desktop.NativeApplication;
	import flash.display.Screen;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.InvokeEvent;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.net.LocalConnection;
	import flash.text.TextField;
	import com.greensock.TweenMax;
	import flash.ui.Mouse;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import uk.org.iwm.kiosk.flexikiosk.log.Log;
	import uk.org.iwm.kiosk.flexikiosk.log.LogConsole;
	import uk.org.iwm.kiosk.flexikiosk.log.LogEvent;
	import uk.org.iwm.kiosk.flexikiosk.log.LogReciever;
	import uk.org.iwm.kiosk.flexikiosk.logger.screens.LogScreen;
	import uk.org.iwm.kiosk.flexikiosk.logger.screens.MainScreen;
	
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class Main extends Sprite 
	{
		private var _process:NativeProcess;
		private var _nativeProcessStartupInfo:NativeProcessStartupInfo;
		
		// consts
		private static const MAIN_SCREEN:String = 'main_screen';
		private static const LOG_SCREEN:String = 'log_screen';
		private static const SETTINGS_SCREEN:String = 'settings_screen';
		
		private var _stageWidth:int;
		private var _stageHeight:int;
		
		private var _currScreen:String = MAIN_SCREEN;
		private var _screens:Sprite;
		private var _mainScreen:MainScreen;
		
		public function Main():void 
		{
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke);
		}
		
		private function onInvoke(e:InvokeEvent):void
		{	
			Mouse.hide();
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			NativeApplication.nativeApplication.activeWindow.x = 0;
			NativeApplication.nativeApplication.activeWindow.y = 0;
			NativeApplication.nativeApplication.activeWindow.width = Screen.mainScreen.bounds.width;
			NativeApplication.nativeApplication.activeWindow.height = Screen.mainScreen.bounds.height;
			
			_stageWidth = Screen.mainScreen.bounds.width;
			_stageHeight = Screen.mainScreen.bounds.height;
			
			var bg:ShapeRect = new ShapeRect(_stageWidth, _stageHeight, 0x252525);
			addChild(bg);
			
			TweenMax.delayedCall(1, buildStage);
			
			stage.focus = stage;
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        }
		
		private function buildStage():void
		{
			stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			_screens = new Sprite();
			addChild(_screens);
			
			_mainScreen = new MainScreen(_stageWidth, _stageHeight);
			_screens.addChild(_mainScreen);
			var logScreen:LogScreen = new LogScreen(_stageWidth, _stageHeight);
			logScreen.x = -1920;
			_screens.addChild(logScreen);
			
			if (NativeProcess.isSupported) {
				_nativeProcessStartupInfo = new NativeProcessStartupInfo(); 
				var file:File = File.applicationDirectory.resolvePath("C:\\Program Files (x86)\\TournamentPlanner\\serproxy\\serproxy.exe"); 
				_nativeProcessStartupInfo.executable = file;
				_process = new NativeProcess(); 
				_process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, onOutputData); 
				_process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, onErrorData); 
				_process.addEventListener(NativeProcessExitEvent.EXIT, onExit); 
				_process.addEventListener(IOErrorEvent.STANDARD_ERROR_IO_ERROR, onIOError); 
				start();
			} else {
				Log.log('Unable to start application due to NativeProcess not available', LogEvent.ERROR);
			}
		}

        private function onOutputData(event:ProgressEvent):void
        {
            Log.log("Status recieved from application " + _process.standardOutput.readUTFBytes(_process.standardOutput.bytesAvailable)); 
        }
        
        private function onErrorData(event:ProgressEvent):void
        {
            Log.log("There was an error with the application. " + _process.standardError.readUTFBytes(_process.standardError.bytesAvailable), LogEvent.ERROR); 
        }
        
        private function onExit(event:NativeProcessExitEvent):void
        {
            Log.log("The application exited. Exit code: " + event.exitCode, LogEvent.WARNING);
			restart();
        }
        
        private function onIOError(event:IOErrorEvent):void
        {
            Log.log(event.toString());
        }
		
		private function start():void
		{
			Log.log("Starting application in 10...");
			TweenMax.killDelayedCallsTo(doRestart);
			TweenMax.delayedCall(10, doRestart);
			_mainScreen.loading = true;
		}
		
		private function restart():void
		{
			Log.log("Restarting application in 10...");
			TweenMax.killDelayedCallsTo(doRestart);
			TweenMax.delayedCall(10, doRestart);
			_mainScreen.loading = true;
		}
		
		private function doRestart():void
		{
			_process.start(_nativeProcessStartupInfo);
			_mainScreen.loading = false;
		}
		
		// Keyboard Events
		private function onKeyDown(e:KeyboardEvent):void
		{
			if (e.keyCode == KeyCode.LEFT) {
				gotoLogScreen();
			} else if (e.keyCode == KeyCode.RIGHT) {
				gotoMainScreen();
			} else if (e.keyCode == KeyCode.SPACE) {
				
			}
		}
		
		// Screen Navigation
		private function gotoMainScreen():void
		{
			if (_currScreen == 'MAIN_SCREEN') return;
			_currScreen = 'MAIN_SCREEN';
			TweenMax.to(_screens, 1, { x:0 } );
		}
		
		private function gotoLogScreen():void
		{
			if (_currScreen == 'LOG_SCREEN') return;
			_currScreen = 'LOG_SCREEN';
			TweenMax.to(_screens, 1, { x:_stageWidth } );
		}
		
		private function gotoSettingsScreen():void
		{
			if (_currScreen == 'SETTINGS_SCREEN') return;
			_currScreen = 'SETTINGS_SCREEN';
			TweenMax.to(_screens, 1, { x:-_stageWidth } );
		}
		
	}
	
}