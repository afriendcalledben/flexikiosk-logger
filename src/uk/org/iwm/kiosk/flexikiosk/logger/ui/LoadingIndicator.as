package uk.org.iwm.kiosk.flexikiosk.logger.ui 
{
	import flash.display.Sprite;
	import com.greensock.TweenMax;
	/**
	 * ...
	 * @author Ben Tandy
	 */
	public class LoadingIndicator extends Sprite
	{
		public function LoadingIndicator() 
		{
			for (var i:int = 0; i < 5; i++) {
				var circ:Sprite = new Sprite();
				circ.graphics.beginFill(0xFFFFFF);
				circ.graphics.drawCircle(0, 0, 6);
				circ.graphics.endFill();
				circ.name = 'circ' + i;
				circ.x = 3 + (i * 30);
				circ.y = 3;
				circ.alpha = 0.1;
				addChild(circ);
			}
			
			startAnimate();
		}
		
		public function startAnimate():void
		{
			animate();
		}
		
		private function animate():void
		{
			for (var i:int = 0; i < 5; i++) {
				var circ:Sprite = Sprite(getChildByName('circ' + i));
				TweenMax.killTweensOf(circ);
				circ.alpha = 0.1;
				TweenMax.to(circ, 0.2, { alpha: 1, delay:1 + (i * 1) } );
				//TweenMax.to(circ, 0.2, { alpha:0.3, delay:1, overwrite:false } );
			}
			TweenMax.delayedCall(10, animate);
		}
		
		public function stopAnimate():void
		{
			TweenMax.killDelayedCallsTo(animate);
			for (var i:int = 0; i < 5; i++) {
				var circ:Sprite = Sprite(getChildByName('circ' + i));
				TweenMax.killTweensOf(circ);
				circ.alpha = 0.1;
			}
		}
		
	}

}